# satnogs-kirfs

`satnogs-kirfs`(keep it really fucking simple) is a C++17 program for 
executing SatNOGS observations and interacting with the SatNOGS Network.

In the dark ages that everything without reason has to be implemented in Python,
some brave souls stand together!
Using the power of C++17 and all of its new goodies, this program is an attempt
to provide a simple, but powerful and highly customizable SatNOGS client.