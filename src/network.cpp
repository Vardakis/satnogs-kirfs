/*
 * network.cpp
 *
 *  Created on: Apr 15, 2020
 *      Author: surligas
 */

#define CPPHTTPLIB_OPENSSL_SUPPORT 1

#include "network.h"
#include "httplib.h"
#include <nlohmann/json.hpp>

/**
 * The constructor stores internally all the necessary information required
 * for communicating with the SatNOGS network
 *
 * @note a newly constructed object does not make any network traffic or
 * transaction
 * @param uri the URI of the service (e.g satnogs.org)
 * @param port the port of the service
 * @param station_id the station ID
 * @param key the API key
 */
network::network(const std::string &uri, uint16_t port, size_t station_id,
                 const std::string &key):
  d_uri(uri),
  d_port(port),
  d_station_id(station_id),
  d_key(key)
{
}

network::~network()
{
}

void
network::fetch_jobs()
{
  httplib::Headers headers = {
    { "Authorization", "Token " + d_key }
  };
  httplib::SSLClient cli(d_uri, d_port);
  const std::string get_str("/api/jobs/?ground_station=" + std::to_string(
                              d_station_id));
  auto res = cli.Get(get_str.c_str(), headers);
  try {
    if (res && res->status == 200) {
      nlohmann::json j = nlohmann::json::parse(res->body);
      std::cout << j << std::endl;
    }
  }
  catch (nlohmann::json::parse_error &e) {
  }
}
