/*
 * network.h
 *
 *  Created on: Apr 15, 2020
 *      Author: surligas
 */

#ifndef SRC_NETWORK_H_
#define SRC_NETWORK_H_

#include <string>
#include <nlohmann/json.hpp>

/**
 * The network class is responsible for the communication with the SatNOGS
 * Network. This includes fetching observations and transmitter infromation,
 * posting back decoded data, etc.
 */
class network {
public:
  network(const std::string &uri, uint16_t port,
          size_t station_id,
          const std::string &key);

  ~network();

  void
  fetch_jobs();

private:
  const std::string d_uri;
  const uint16_t d_port;
  const size_t d_station_id;
  const std::string d_key;
};

#endif /* SRC_NETWORK_H_ */
