/*
 * observation.h
 *
 *  Created on: Apr 15, 2020
 *      Author: surligas
 */

#ifndef SRC_OBSERVATION_H_
#define SRC_OBSERVATION_H_

#include <chrono>
#include <string>

class observation {
public:
  observation();
  virtual
  ~observation();
private:
  const size_t d_id;
  const size_t d_station_id;
  const std::string d_tle0;
  const std::string d_tle1;
  const std::string d_tle2;
  const std::string d_mode;
  const std::string d_transmitter;
  const double d_frequency;
  const double d_baudrate;
  std::chrono::time_point d_start;
  std::chrono::time_point d_stop;
};

#endif /* SRC_OBSERVATION_H_ */
